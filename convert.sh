#!/bin/bash
d="${PWD}/squirtle/"
count=0
for f in `find $d -type f -name "*"` ; do
	count=$((count+1))
        filename=$(basename $f)
        echo "Now Processing File: ${filename}"
	convert -strip "${d}${filename}" "${d}edited/${filename}"
done
echo "${count}"

