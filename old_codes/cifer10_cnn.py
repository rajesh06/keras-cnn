
##
# Train a simple deep CNN on the CIFAR10 small images dataset.
# It gets to 75% validation accuracy in 25 epochs, and 79% after 50 epochs.
# (it's still under fitting at that point, though).
##


from __future__ import print_function
import os
# import numpy as np

from keras.datasets import cifar10
from keras.optimizers import rmsprop
from keras.utils import to_categorical
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Dropout

batch_size = 256
num_classes = 10
epochs = 10
data_augmentation = True
num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'
data_gen = ImageDataGenerator(
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images


# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
input_shape = x_train.shape[1:]
y_train = to_categorical(y_train, num_classes)
y_test = to_categorical(y_test, num_classes)


def create_model():
    model = Sequential()

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(MaxPool2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))

    return model


model1 = create_model()

# initiate RMSprop optimizer
opt = rmsprop(lr=0.0001, decay=1e-6)

model1.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

history = model1.fit_generator(data_gen.flow(x_train, y_train, batch_size=batch_size),
                               epochs=epochs,
                               validation_data=(x_test, y_test),
                               workers=4)
model1.evaluate(x_test, y_test)
